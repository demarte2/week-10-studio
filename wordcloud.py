import sys


background_color = "#101010"
height = 720
width = 1080
#creates a list for words in .txt
wordlist = {}
#prevents these words from being counted (because they would be the msot commonly used words)
stoplist = ["a", "an", "the", "I", "of", "from", "to", "in", "and", "not", "i", "my", "is"]
#loop for counting words in .txt
with open(sys.argv[1], "r") as f:
    for line in f:
        words = line.split()
        for word in words:
            word = word.lower()
            if word not in stoplist:
                wordlist.setdefault(word, 0)
                wordlist[word] += 1

with open(sys.argv[2], "a+") as f:
    print("***", sys.argv[1], file=f)
    i = 0
    for word in sorted(wordlist, key=wordlist.get, reverse=True):
        print(word + ": " + str(wordlist[word]), file=f)
        i = i + 1
        if i >= 10:
            break

from wordcloud import WordCloud

# image configurations
background_color = "#101010"
height = 720
width = 1080

with open("stopwords.txt", "r") as f:
    stop_words = f.read().split()

# Read a text file and calculate frequency of words in it
with open("/tmp/sample_text.txt", "r") as f:
    words = f.read().split()

data = dict()

for word in words:
    word = word.lower()
    if word in stop_words:
        continue

    data[word] = data.get(word, 0) + 1

word_cloud = WordCloud(
    background_color=background_color,
    width=width,
    height=height
)

word_cloud.generate_from_frequencies(data)
word_cloud.to_file('image.png')
