# Week 10 Studio

A word cloud is a way of visualizing some text. If you have some text – like a song, a speech, or a document – you can use the word cloud to see approximately what that text is about and what kinds of things are being talked about.