import sys
from turtle import *

wordlist = {}

stoplist = ["a", "an", "the", "I", "of", "from", "to", "in", "and", "not", "i", "my", "is", "me"]

with open(sys.argv[1], "r") as f:
    for line in f:
        words = line.split()
        for word in words:
            word = word.lower()
            if word not in stoplist:
                wordlist.setdefault(word, 0)
                wordlist[word] += 1

#with open(sys.argv[2], "a+") as f:
    #print("***", sys.argv[1], file=f)
    #i = 0
    #for word in sorted(wordlist, key=wordlist.get, reverse=True):
        #print(word, file=f)
        #i = i + 1
        #if i >= 10:
            #break

#Top_10_list = []


for j in range(10):
    write(wordlist)
    pu()
    right(90)
    forward(20)
    j = j+1
    if j >= 10:
        done()

done()
